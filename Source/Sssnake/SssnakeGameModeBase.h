// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SssnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SSSNAKE_API ASssnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
