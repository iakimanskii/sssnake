// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SSSNAKE_SssnakeGameModeBase_generated_h
#error "SssnakeGameModeBase.generated.h already included, missing '#pragma once' in SssnakeGameModeBase.h"
#endif
#define SSSNAKE_SssnakeGameModeBase_generated_h

#define Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_SPARSE_DATA
#define Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_RPC_WRAPPERS
#define Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASssnakeGameModeBase(); \
	friend struct Z_Construct_UClass_ASssnakeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASssnakeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Sssnake"), NO_API) \
	DECLARE_SERIALIZER(ASssnakeGameModeBase)


#define Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASssnakeGameModeBase(); \
	friend struct Z_Construct_UClass_ASssnakeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASssnakeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Sssnake"), NO_API) \
	DECLARE_SERIALIZER(ASssnakeGameModeBase)


#define Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASssnakeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASssnakeGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASssnakeGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASssnakeGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASssnakeGameModeBase(ASssnakeGameModeBase&&); \
	NO_API ASssnakeGameModeBase(const ASssnakeGameModeBase&); \
public:


#define Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASssnakeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASssnakeGameModeBase(ASssnakeGameModeBase&&); \
	NO_API ASssnakeGameModeBase(const ASssnakeGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASssnakeGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASssnakeGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASssnakeGameModeBase)


#define Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Sssnake_Source_Sssnake_SssnakeGameModeBase_h_12_PROLOG
#define Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_SPARSE_DATA \
	Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_RPC_WRAPPERS \
	Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_INCLASS \
	Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_SPARSE_DATA \
	Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Sssnake_Source_Sssnake_SssnakeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SSSNAKE_API UClass* StaticClass<class ASssnakeGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Sssnake_Source_Sssnake_SssnakeGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
