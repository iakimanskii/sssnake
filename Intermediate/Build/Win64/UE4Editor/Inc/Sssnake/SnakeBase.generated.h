// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SSSNAKE_SnakeBase_generated_h
#error "SnakeBase.generated.h already included, missing '#pragma once' in SnakeBase.h"
#endif
#define SSSNAKE_SnakeBase_generated_h

#define Sssnake_Source_Sssnake_SnakeBase_h_14_SPARSE_DATA
#define Sssnake_Source_Sssnake_SnakeBase_h_14_RPC_WRAPPERS
#define Sssnake_Source_Sssnake_SnakeBase_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Sssnake_Source_Sssnake_SnakeBase_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeBase(); \
	friend struct Z_Construct_UClass_ASnakeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Sssnake"), NO_API) \
	DECLARE_SERIALIZER(ASnakeBase)


#define Sssnake_Source_Sssnake_SnakeBase_h_14_INCLASS \
private: \
	static void StaticRegisterNativesASnakeBase(); \
	friend struct Z_Construct_UClass_ASnakeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Sssnake"), NO_API) \
	DECLARE_SERIALIZER(ASnakeBase)


#define Sssnake_Source_Sssnake_SnakeBase_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeBase(ASnakeBase&&); \
	NO_API ASnakeBase(const ASnakeBase&); \
public:


#define Sssnake_Source_Sssnake_SnakeBase_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeBase(ASnakeBase&&); \
	NO_API ASnakeBase(const ASnakeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeBase)


#define Sssnake_Source_Sssnake_SnakeBase_h_14_PRIVATE_PROPERTY_OFFSET
#define Sssnake_Source_Sssnake_SnakeBase_h_11_PROLOG
#define Sssnake_Source_Sssnake_SnakeBase_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Sssnake_Source_Sssnake_SnakeBase_h_14_PRIVATE_PROPERTY_OFFSET \
	Sssnake_Source_Sssnake_SnakeBase_h_14_SPARSE_DATA \
	Sssnake_Source_Sssnake_SnakeBase_h_14_RPC_WRAPPERS \
	Sssnake_Source_Sssnake_SnakeBase_h_14_INCLASS \
	Sssnake_Source_Sssnake_SnakeBase_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Sssnake_Source_Sssnake_SnakeBase_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Sssnake_Source_Sssnake_SnakeBase_h_14_PRIVATE_PROPERTY_OFFSET \
	Sssnake_Source_Sssnake_SnakeBase_h_14_SPARSE_DATA \
	Sssnake_Source_Sssnake_SnakeBase_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Sssnake_Source_Sssnake_SnakeBase_h_14_INCLASS_NO_PURE_DECLS \
	Sssnake_Source_Sssnake_SnakeBase_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SSSNAKE_API UClass* StaticClass<class ASnakeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Sssnake_Source_Sssnake_SnakeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
