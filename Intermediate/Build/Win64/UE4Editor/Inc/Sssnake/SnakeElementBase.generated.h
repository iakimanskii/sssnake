// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SSSNAKE_SnakeElementBase_generated_h
#error "SnakeElementBase.generated.h already included, missing '#pragma once' in SnakeElementBase.h"
#endif
#define SSSNAKE_SnakeElementBase_generated_h

#define Sssnake_Source_Sssnake_SnakeElementBase_h_14_SPARSE_DATA
#define Sssnake_Source_Sssnake_SnakeElementBase_h_14_RPC_WRAPPERS
#define Sssnake_Source_Sssnake_SnakeElementBase_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Sssnake_Source_Sssnake_SnakeElementBase_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeElementBase(); \
	friend struct Z_Construct_UClass_ASnakeElementBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeElementBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Sssnake"), NO_API) \
	DECLARE_SERIALIZER(ASnakeElementBase)


#define Sssnake_Source_Sssnake_SnakeElementBase_h_14_INCLASS \
private: \
	static void StaticRegisterNativesASnakeElementBase(); \
	friend struct Z_Construct_UClass_ASnakeElementBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeElementBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Sssnake"), NO_API) \
	DECLARE_SERIALIZER(ASnakeElementBase)


#define Sssnake_Source_Sssnake_SnakeElementBase_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeElementBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeElementBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeElementBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeElementBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeElementBase(ASnakeElementBase&&); \
	NO_API ASnakeElementBase(const ASnakeElementBase&); \
public:


#define Sssnake_Source_Sssnake_SnakeElementBase_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeElementBase(ASnakeElementBase&&); \
	NO_API ASnakeElementBase(const ASnakeElementBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeElementBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeElementBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeElementBase)


#define Sssnake_Source_Sssnake_SnakeElementBase_h_14_PRIVATE_PROPERTY_OFFSET
#define Sssnake_Source_Sssnake_SnakeElementBase_h_11_PROLOG
#define Sssnake_Source_Sssnake_SnakeElementBase_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Sssnake_Source_Sssnake_SnakeElementBase_h_14_PRIVATE_PROPERTY_OFFSET \
	Sssnake_Source_Sssnake_SnakeElementBase_h_14_SPARSE_DATA \
	Sssnake_Source_Sssnake_SnakeElementBase_h_14_RPC_WRAPPERS \
	Sssnake_Source_Sssnake_SnakeElementBase_h_14_INCLASS \
	Sssnake_Source_Sssnake_SnakeElementBase_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Sssnake_Source_Sssnake_SnakeElementBase_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Sssnake_Source_Sssnake_SnakeElementBase_h_14_PRIVATE_PROPERTY_OFFSET \
	Sssnake_Source_Sssnake_SnakeElementBase_h_14_SPARSE_DATA \
	Sssnake_Source_Sssnake_SnakeElementBase_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Sssnake_Source_Sssnake_SnakeElementBase_h_14_INCLASS_NO_PURE_DECLS \
	Sssnake_Source_Sssnake_SnakeElementBase_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SSSNAKE_API UClass* StaticClass<class ASnakeElementBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Sssnake_Source_Sssnake_SnakeElementBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
